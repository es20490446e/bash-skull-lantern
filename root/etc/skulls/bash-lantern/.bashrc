#! /bin/bash
## Part of: bash-skull-lantern
## License: Latest GNU Affero


#############
## CONFIGS ##
#############


bash_config () {
	if bash_terminalIsInteractive; then
		bash_configureTerminalBehavior
		bash_importCompletions
		bash_setEditors
		bash_setColors
		PROMPT_COMMAND="bash_prompt"
	fi
}


bash_terminalIsInteractive () {
	[[ $- == *i* ]]
}


bash_configureTerminalBehavior () {
	export HISTCONTROL="erasedups:ignorespace"
	export PATH="${HOME}/.local/bin:${PATH}"
	shopt -s expand_aliases
	shopt -s checkwinsize
	shopt -s histappend
}


bash_importCompletions () {
	if [[ -r /usr/share/bash-completion/bash_completion ]] && ! shopt -oq posix; then
		. /usr/share/bash-completion/bash_completion

		if [[ "$(expr index "$-" i)" > 0 ]]; then
			bind "set completion-ignore-case on"
			bind "set show-all-if-ambiguous On"
		fi
	fi
}


bash_setEditors () {
	export EDITOR="nano"
	bash_setVisualEditor
}


bash_setVisualEditor () {
	local editorInConfig; editorInConfig="$(
		grep "application/x-zerosize" "${HOME}/.config/mimeapps.list" 2>/dev/null |
		cut --delimiter='=' --fields=2 |
		tr -d ';' |
		rev |
		cut --delimiter='.' --fields=2 |
		rev
	)"

	if [[ -z "${editorInConfig}" ]] || [[ ! -x "/bin/${editorInConfig}" ]]; then
		export VISUAL="${EDITOR}"
	else
		export VISUAL="${editorInConfig}"
	fi
}


bash_setColors () {
	export COLORC_="\033[0m"
	export COLORC_B="\033[0;30m"
	export COLORC_BB="\033[1;30m"
	export COLORC_R="\033[0;31m"
	export COLORC_RB="\033[1;31m"
	export COLORC_G="\033[0;32m"
	export COLORC_GB="\033[1;32m"
	export COLORC_Y="\033[0;33m"
	export COLORC_YB="\033[1;33m"
	export COLORC_B="\033[0;34m"
	export COLORC_BB="\033[1;34m"
	export COLORC_O="\033[0;35m"
	export COLORC_OB="\033[1;35m"
	export COLORC_P="\033[0;36m"
	export COLORC_PB="\033[1;36m"
	export COLORC_W="\033[0;37m"
	export COLORC_WB="\033[1;37m"
}


bash_prompt () {
	local status="$?"

	echo -ne "\033]0;${PWD##*/} @ ${HOSTNAME}\007"

	if [[ "${status}" != 0 ]]; then
		PS1="\[${COLORC_RB}\]✗ ${status}\n"
		bash_tone &
		disown "$!"
	elif [[ -n "${PS0}" ]]; then
		PS1="\[${COLORC_GB}\]✔\n"
		bash_tone &
		disown "$!"
	else
		PS1=""
	fi

	if [[ -z "${PS0}" ]]; then
		PS0="\[${COLORC_}\]"
	else
		PS1+="\[${COLORC_}\]\n"
	fi

	if [[ "${EUID}" -eq 0 ]] ; then
		PS1+="\[${COLORC_YB}\]🔓 command: \[${COLORC_OB}\]"
		PS2="\[${COLORC_YB}\]🔓 .......: \[${COLORC_OB}\]"
	else
		PS1+="\[${COLORC_BB}\]command: \[${COLORC_OB}\]"
		PS2="\[${COLORC_BB}\].......: \[${COLORC_OB}\]"
	fi
}


bash_tone () {
	play -q -n -c2 synth sin 130 sin 131 sin 801 sin 800 fade t 0.1 0.3 vol 0.75 &>/dev/null || true
}


bash_config


##########
## MODS ##
##########

# If you want to add extra commands
# consider making them separate programs instead


# env searches environment variables too
env () {
	local args=("${@}")

	if [[ "${#args[@]}" -eq 0 ]]; then
		/bin/env |
		sort
	elif  [[ "${args:0:1}" == "-" ]]; then
		/bin/env "${args[@]}"
	else
		local value; value="$(
			/bin/env |
			grep "${args[*]}" |
			sort
		)"

		if [[ -z "${value}" ]]; then
			return 1
		else
			echo "${value}"
		fi
	fi
}
