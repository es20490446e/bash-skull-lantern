#! /bin/bash


Template="/etc/skulls/bash-lantern/.bashrc"


Generateds=(
	"/etc/bash.bashrc"
	"/etc/skel/.bashrc"
)


post_install () {
	skull bash-lantern
	OverrideFiles
}


pre_upgrade () {
	RemoveNonModifiedFiles
}


post_upgrade () {
	skull bash-lantern
	ClobberFiles
}


ClobberFiles () {
	local Generated
	
	for Generated in "${Generateds[@]}"; do
		rm --force "${Generated}."*
	
		if [[ ! -f "${Generated}" ]]; then
			cp --force "${Template}" "${Generated}"
		fi
	done
}


OverrideFiles () {
	local Generated
	
	for Generated in "${Generateds[@]}"; do
		if compgen -G "${Generated}."* >/dev/null; then
			mv "${Generated}."* "${Generated}"
		else
			cp "${Template}" "${Generated}"
		fi
	done
}


RemoveNonModifiedFiles () {
	InstallationDate="$(stat --format="%Y" "${Template}")"
	local Generated
	
	for Generated in "${Generateds[@]}"; do
		if [[ -f "${Generated}" ]] && [[ "$(stat --format="%Y" "${Generated}")" -eq "${InstallationDate}" ]]; then
			rm "${Generated}"
		fi
	done
}
